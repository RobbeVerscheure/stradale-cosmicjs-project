"use script"

document.addEventListener("DOMContentLoaded",init);


function init(){
    loadBlogPreviews();
    document.querySelector("#postsOverview").addEventListener("click",showContent)
}

function loadBlogPreviews(){
    fetch('https://api.cosmicjs.com/v1/stradale-comeback/objects?pretty=true&hide_metafields=true&type=indices&read_key={KEY_HERE}')

        .then(response => response.json())
        .then(data => {

            fillBlogPreviews(data);
        });
}

function fillBlogPreviews(data){
    let div = document.querySelector("#postsOverview");
    let posts = data["objects"];

    div.innerHTML = "";
    posts.forEach(function (post){

        div.innerHTML+= `<div id="${post.slug}" class='post'><figure><img alt="" src="${post.thumbnail}"><figcaption>${post.title}</figcaption>
                            </figure><a class="postLink" >Read this -></a><div class="${post.slug} hidden">${post.content}<a class="closePost">Hide content</a></div><p>${getDate(post.created)}</p>
                            </div>`
    });
}

function getDate(date){
    return date.substr(0,10);
}

function showContent(e){
    console.log(e.target);

    if (e.target.classList.contains("postLink")){
       let slug = e.target.parentElement.id;


        document.querySelector(`.${slug}`).classList.remove("hidden");

        console.log("got rid of hidden");
    } else if(e.target.classList.contains("closePost")){

        let slug = e.target.parentElement.classList[0];
        document.querySelector(`.${slug}`).classList.add("hidden");
    }

}

