"use strict"

document.addEventListener("DOMContentLoaded",init);


function init(){
    loadInPictures();
}

function loadInPictures(){
    fetch('https://api.cosmicjs.com/v1/stradale-comeback/objects?pretty=true&hide_metafields=true&type=photos&read_key=W5C5tVOqwgPubOuRhyAdYx5kI6Zn4GatKW8ZIUJb9nVWCxANTD&limit=20&props=slug,title,metadata,')

        .then(response => response.json())
        .then(data => {
            fillPictures(data);
        });
}


function fillPictures(data){

    let div =document.querySelector(".grid-container");
    let objects = data.objects;


    div.innerHTML=  `<div class="Photo1 photo"><img alt="${objects[0].title}" src="${objects[0].metadata.image.url}"><div class="info"><span>Location: ${objects[0].metadata.location}</span><span  class="date">Taken at: ${objects[0].metadata.date}</span></div></div>
    <div class="photo Photo2 "><img id="old" alt="${objects[1].title}" src="${objects[1].metadata.image.url}"><div class="info"><span>Location: ${objects[1].metadata.location}</span><span class="date">Taken at: ${objects[1].metadata.date}</span></div></div>
    <div class="photo Photo3 "><img alt="${objects[2].title}" src="${objects[2].metadata.image.url}"><div class="info"><span >Location: ${objects[2].metadata.location}</span><span    class="date">Taken at: ${objects[2].metadata.date}</span></div></div>
    <div class="photo Photo4 "><img alt="${objects[4].title}" src="${objects[4].metadata.image.url}"><div class="info"><span >Location: ${objects[4].metadata.location}</span><span  class="date">Taken at: ${objects[4].metadata.date}</span></div></div>
    <div class="photo Photo5 "><img alt="${objects[5].title}" src="${objects[5].metadata.image.url}"><div class="info"><span >Location: ${objects[5].metadata.location}</span><span class="date">Taken at: ${objects[5].metadata.date}</span></div></div>
    <div class=" photo Photo6 "><img id="bottom" alt="${objects[6].title}" src="${objects[6].metadata.image.url}"><div class="info"><span>Location: ${objects[6].metadata.location}</span><span class="date">Taken at: ${objects[6].metadata.date}</span></div></div>`



}