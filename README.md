# Stradale - CosmicJS project

A school assignment. Me and some fellow students had to make a portfolio using the headless cms system [CosmicJS](https://www.cosmicjs.com/).

## CosmicJS as the source of content
In this project, we used CosmicJS as our source of content. In CosmicJS, we were able to add our own data.

### Photos
On the website, you can find some pictures of cars. These pictures are stored on the Cosmic servers and can be fetched using an API.
The pictures themselves are stored on CosmicJS.

These pictures can easily be fetched using this url:

`https://api.cosmicjs.com/v1/stradale-comeback/objects?pretty=true&hide_metafields=true&type=photos&read_key={INSERT_PERSONAL_KEY}`

### BlogPosts
The posts on the blog are also stored on CosmicJS. Just like the photos, these posts are also fetchable using an API.

`https://api.cosmicjs.com/v1/stradale-comeback/objects?pretty=true&hide_metafields=true&type=indices&read_key={INSERT_KEY_HERE}` 

## Stradale Website
The [Stradale website](https://stradale.robbeverscheure.be/) is a simple site existing of just plain HTML and Javascript. On load, the different API fetches are executed to give our site the nice content it deserves.
